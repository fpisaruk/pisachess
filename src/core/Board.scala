package core

import core._

object Board {

  def apply(): Board = {
    new Board(initial)
  }

  def apply(pieceByPosition: scala.collection.immutable.Map[Position, Piece]): Board = {
    new Board(pieceByPosition)
  }

  def apply(pieceByPosition: scala.collection.immutable.Map[Position, Piece], moves: List[Move]): Board = {
    new Board(pieceByPosition, moves)
  }

  private def initial(): scala.collection.immutable.Map[Position, Piece] = {
    scala.collection.immutable.Map(
      Position('a', 1) -> Piece("♖"),
      Position('b', 1) -> Piece("♘"),
      Position('c', 1) -> Piece("♗"),
      Position('d', 1) -> Piece("♕"),
      Position('e', 1) -> Piece("♔"),
      Position('f', 1) -> Piece("♗"),
      Position('g', 1) -> Piece("♘"),
      Position('h', 1) -> Piece("♖"),

      Position('a', 8) -> Piece("♜"),
      Position('b', 8) -> Piece("♞"),
      Position('c', 8) -> Piece("♝"),
      Position('d', 8) -> Piece("♛"),
      Position('e', 8) -> Piece("♚"),
      Position('f', 8) -> Piece("♝"),
      Position('g', 8) -> Piece("♞"),
      Position('h', 8) -> Piece("♜"),

      Position('a', 7) -> Piece("♟"),
      Position('b', 7) -> Piece("♟"),
      Position('c', 7) -> Piece("♟"),
      Position('d', 7) -> Piece("♟"),
      Position('e', 7) -> Piece("♟"),
      Position('f', 7) -> Piece("♟"),
      Position('g', 7) -> Piece("♟"),
      Position('h', 7) -> Piece("♟"),

      Position('a', 2) -> Piece("♙"),
      Position('b', 2) -> Piece("♙"),
      Position('c', 2) -> Piece("♙"),
      Position('d', 2) -> Piece("♙"),
      Position('e', 2) -> Piece("♙"),
      Position('f', 2) -> Piece("♙"),
      Position('g', 2) -> Piece("♙"),
      Position('h', 2) -> Piece("♙"))
  }

}

class Board(private val pieceByPosition: scala.collection.immutable.Map[Position, Piece], val moves: List[Move]) {

  def this(pieceByPosition: scala.collection.immutable.Map[Position, Piece]) {
    this(pieceByPosition, List[Move]())
  }

  //FIXME do not user var :-(
  private var positionsByPiece = scala.collection.mutable.Map[Piece, List[Position]]();

  pieceByPosition foreach {
    case (position, piece) => {
      if (positionsByPiece.contains(piece)) {
        positionsByPiece(piece) = positionsByPiece(piece) :+ position
      } else {
        positionsByPiece(piece) = List(position)
      }
    }
  }

  def checkMove(move: Move): Boolean = {
    if (move == null || !positionsByPiece.contains(move.piece))
      return false
    val positions = positionsByPiece(move.piece)

    val piece = move.piece
    val destination = move.destination
    if (positions.filter { origin =>
      {
        (move.fileOfDeparture == "" || move.fileOfDeparture.charAt(0) == origin.file) &&
          (move.rankOfDeparture == "" || move.rankOfDeparture.toInt == origin.rank) &&
          piece.canMove(origin, destination, this)
      }
    }.length != 1) {
      return false
    }
    if (opponent_at(destination, piece.player) && (!move.capture && move.promoteTo == null))
      return false
    return true
  }

  def applyMove(move: Move): Board = {
    val positions = positionsByPiece(move.piece)
    val piece = move.piece
    val destination = move.destination

    move match {
      case _: QueenSideCastling => {
        //FIXME move this logic to related case class
        val kingOrigin = positionsByPiece(move.piece)(0)
        val rookOrigin = positionsByPiece(Rook(move.piece.player)).find(pos => pos.file == 'a').get
        val rookDestination = Position('d', kingOrigin.rank)
        Board(pieceByPosition + { destination -> piece } - kingOrigin + { rookDestination -> Rook(move.piece.player) } - rookOrigin, (this.moves :+ Move(move, kingOrigin)))
      }
      case _: KingSideCastling => {
        //FIXME move this logic to related case class
        val kingOrigin = positionsByPiece(move.piece)(0)
        val rookOrigin = positionsByPiece(Rook(move.piece.player)).find(pos => pos.file == 'h').get
        val rookDestination = Position('f', kingOrigin.rank)
        Board(pieceByPosition + { destination -> piece } - kingOrigin + { rookDestination -> Rook(move.piece.player) } - rookOrigin, (this.moves :+ Move(move, kingOrigin)))
      }
      case _: Move => {
        val origin = positions.find { pos =>
          {
            (move.fileOfDeparture == "" || move.fileOfDeparture.charAt(0) == pos.file) &&
              (move.rankOfDeparture == "" || move.rankOfDeparture.toInt == pos.rank) &&
              piece.canMove(pos, destination, this)
          }
        }.get
        //FIXME create promotion move
        if (move.promoteTo != null) {
          Board(pieceByPosition + { destination -> move.promoteTo } - origin, (this.moves :+ Move(move, origin)))
        } else {
          //FIXME en passant should not be treated here. Piece.canMove should provide a move class instance...
          if (empty(destination) && move.capture) {
            println("it is an en passant")
            val direction = Direction(origin, destination)
            val opponentPawnPosition = if (direction == ↖() || direction == ↙()) ⇠().to(origin, 1)
            else
              ⇢().to(origin, 1)
            Board(pieceByPosition + { destination -> piece } - origin - opponentPawnPosition, (this.moves :+ Move(move, origin)))
          } else
            Board(pieceByPosition + { destination -> piece } - origin, (this.moves :+ Move(move, origin)))
        }
      }
    }
  }

  def check(player: Player): Boolean = {
    val myKingPos = positionsByPiece(King(player))(0)
    attackableByOpponent(myKingPos, player)
  }

  def attackableByOpponent(position: Position, player: Player): Boolean = {
    pieceByPosition.exists(pieceAndPosition => {
      val capture = Move(pieceAndPosition._2, position, true)
      pieceAndPosition._2.player == player.opponent && checkMove(capture)
    })
  }

  def reachableByOpponent(position: Position, player: Player): Boolean = {
    pieceByPosition.exists(pieceAndPosition => {
      pieceAndPosition._2.player == player.opponent &&
        checkMove(Move(pieceAndPosition._2, position, false))
    })
  }

  //TODO test each piece role once
  def checkMate(player: Player): Boolean = {
    val myKingPos = positionsByPiece(King(player))(0)
    !pieceByPosition.exists(pieceAndPosition => {
      val piece = pieceAndPosition._2
      val destination = Position.all.find(destination => {
        val move = Move(piece, destination, false)
        val capture = Move(piece, destination, true)
        (checkMove(move) && !applyMove(move).check(player)) ||
          (checkMove(capture) && !applyMove(capture).check(player))
      })
      // println("move piece " + piece + " to " + destination)
      piece.player == player &&
        Position.all.exists(destination => {
          val move = Move(piece, destination, false)
          val capture = Move(piece, destination, true)
          (checkMove(move) && !applyMove(move).check(player)) ||
            (checkMove(capture) && !applyMove(capture).check(player))
        })
    })
  }

  override def toString(): String = {
    val empty_space = 0x3000.toChar.toString()

    "\n".concat(
      Position.ranks.reverse.map(
        rank => rank + " | " + Position.files.map(
          file => {
            if (pieceByPosition.contains(Position(file, rank)))
              pieceByPosition(Position(file, rank)) + " | "
            else empty_space + " | "
          }).mkString("")).mkString("\n   ------------------------------------\n")).
      concat("\n").concat("\n  | A | B  | C  | D |  E | F |  G | H  |")
  }

  def getPieceByPosition(position: Position): Piece = {
    if (pieceByPosition contains position)
      pieceByPosition(position)
    else
      null
  }

  def getPieceByDestination(destination: Position, piece: String, player: Player): Piece = {
    Piece(piece, player)
  }

  def empty(pos: Position): Boolean = {
    getPieceByPosition(pos) == null
  }

  def opponent_at(pos: Position, myPlayer: Player): Boolean = {
    if (empty(pos)) return false
    getPieceByPosition(pos).player == myPlayer.opponent
  }

  def empty_or_opponent_at(pos: Position, myColor: Player): Boolean = {
    empty(pos) || opponent_at(pos, myColor);
  }

  def blocked(direction: Direction, origin: Position, destination: Position): Boolean = {
    origin.positionsTo(destination) exists { position =>
      !empty(position)
    }
  }
}


