package core

abstract class Direction {
  override def toString(): String = new String(Character.toChars(Integer.parseInt(getClass.getName().substring(2), 16)));
  def to(origin: Position, length: Int): Position
}

object Direction {
  def apply(origin: Position, destination: Position): Direction = {
    (origin, destination) match {
      case (Position(of, or), Position(df, dr)) if ((dr - or > 0) && (df == of)) => ⇡()
      case (Position(of, or), Position(df, dr)) if ((dr - or < 0) && (df == of)) => ⇣()
      case (Position(of, or), Position(df, dr)) if ((df - of < 0) && (dr == or)) => ⇠()
      case (Position(of, or), Position(df, dr)) if ((df - of > 0) && (dr == or)) => ⇢()
      case (Position(of, or), Position(df, dr)) if ((-(df - of) == (dr - or)) && (df - of < 0)) => ↖()
      case (Position(of, or), Position(df, dr)) if (((df - of) == -(dr - or)) && (df - of > 0)) => ↘()
      case (Position(of, or), Position(df, dr)) if (((df - of) == (dr - or)) && (df - of < 0)) => ↙()
      case (Position(of, or), Position(df, dr)) if (((df - of) == (dr - or)) && (df - of > 0)) => ↗()
      case _ => null
    }
  }
}

case class ⇡() extends Direction {
  def to(origin: Position, length: Int): Position = {
    Position(origin.file, origin.rank + length)
  }
}
case class ⇣() extends Direction {
  def to(origin: Position, length: Int): Position = {
    Position(origin.file, origin.rank - length)
  }
}
case class ⇠() extends Direction {
  def to(origin: Position, length: Int): Position = {
    Position((origin.file - length).toChar, origin.rank)
  }
}
case class ⇢() extends Direction {
  def to(origin: Position, length: Int): Position = {
    Position((origin.file + length).toChar, origin.rank)
  }
}
case class ↖() extends Direction {
  def to(origin: Position, length: Int): Position = {
    Position((origin.file - length).toChar, origin.rank + length)
  }
}
case class ↘() extends Direction {
  def to(origin: Position, length: Int): Position = {
    Position((origin.file + length).toChar, origin.rank - length)
  }
}
case class ↙() extends Direction {
  def to(origin: Position, length: Int): Position = {
    Position((origin.file - length).toChar, origin.rank - length)
  }
}
case class ↗() extends Direction {
  def to(origin: Position, length: Int): Position = {
    Position((origin.file + length).toChar, origin.rank + length)
  }
}


