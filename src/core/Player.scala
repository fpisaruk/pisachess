package core

sealed trait Player {
  val opponent: Player
}
case object Black extends Player {
  val opponent = White
}
case object White extends Player {
  val opponent = Black
}