package core

import core._

abstract class Piece(val player: Player, val figurine: String, val algebraic: String) {
  override def toString(): String = figurine
  override def hashCode(): Int = { (figurine).hashCode() }
  override def equals(o: Any) = o match {
    case that: Piece => that.figurine == this.figurine
    case _ => false
  }
  //FIXME canMove must receive an Move type: Capture, Shift, Promotion, Castling
  def canMove(origin: Position, destination: Position, board: Board): Boolean
}

object Piece {
  def apply(piece: String, player: Player): Piece = {
    piece match {
      case "K" => King(player)
      case "Q" => Queen(player)
      case "B" => Bishop(player)
      case "R" => Rook(player)
      case "N" => Knight(player)
      case "" => Pawn(player)
    }
  }

  def apply(piece: String): Piece = {
    piece match {
      case "♟" => Pawn(Black)
      case "♙" => Pawn(White)
      case "♘" => Knight(White)
      case "♞" => Knight(Black)
      case "♔" => King(White)
      case "♚" => King(Black)
      case "♕" => Queen(White)
      case "♛" => Queen(Black)
      case "♗" => Bishop(White)
      case "♝" => Bishop(Black)
      case "♖" => Rook(White)
      case "♜" => Rook(Black)
    }
  }
}

case class Pawn(override val player: Player) extends Piece(player, if (player == White) "♙" else "♟", "") {
  def allowed_move_direction(direction: Direction): Boolean = {
    (player == White && List(⇡()).contains(direction)) ||
      (player == Black && List(⇣()).contains(direction))
  }
  def allowed_capture_direction(direction: Direction): Boolean = {
    (player == White && List(↖(), ↗()).contains(direction)) ||
      (player == Black && List(↙(), ↘()).contains(direction))
  }
  override def canMove(origin: Position, destination: Position, board: Board): Boolean = {
    val initialPosition: Boolean = (player == White && origin.rank == 2) || (player == Black && origin.rank == 7)
    val direction = Direction(origin, destination)
    val distance = destination.distance(origin)

    ((allowed_move_direction(direction) && initialPosition && !board.blocked(direction, origin, destination) && board.empty(destination) && (distance == 2)) ||
      (allowed_move_direction(direction) && board.empty(destination) && (distance == 1)) ||
      (allowed_capture_direction(direction) && board.opponent_at(destination, player) && (distance == 1))) ||
      enPassant(origin, destination, board)
  }

  def enPassant(origin: Position, destination: Position, board: Board): Boolean = {
    if (board.moves.length == 0)
      return false
    val direction = Direction(origin, destination)
    val distance = destination.distance(origin)
    val lastMove = board.moves.last

    (distance == 1 && lastMove.piece == Pawn(player.opponent) && lastMove.distance == 2) && allowed_capture_direction(direction) && (board.empty(destination))
    (
      (direction == ↖() && lastMove.origin == ⇡().to(destination, 1)) ||
      (direction == ↗() && lastMove.origin == ⇡().to(destination, 1)) ||
      (direction == ↙() && lastMove.origin == ⇣().to(destination, 1)) ||
      (direction == ↘() && lastMove.origin == ⇣().to(destination, 1)))
  }

}

case class Knight(override val player: Player) extends Piece(player, if (player == White) "♘" else "♞", "N") {
  override def canMove(origin: Position, destination: Position, board: Board): Boolean = {
    ((
      destination == origin.⇡().⇢(2) || destination == origin.⇡().⇠(2) ||
      destination == origin.⇣().⇢(2) || destination == origin.⇣().⇠(2) ||
      destination == origin.⇡(2).⇢() || destination == origin.⇡(2).⇠() ||
      destination == origin.⇣(2).⇢() || destination == origin.⇣(2).⇠()) &&
      board.empty_or_opponent_at(destination, player))
  }
}

case class King(override val player: Player) extends Piece(player, if (player == White) "♔" else "♚", "K") {
  def allowed_directon(direction: Direction): Boolean = {
    List(↖(), ↘(), ↙(), ↗(), ⇡(), ⇣(), ⇠(), ⇢()).contains(direction)
  }
  def initialPosition: Position = {
    val rank = if (player == White)
      1
    else
      8
    Position('e', rank)
  }

  override def canMove(origin: Position, destination: Position, board: Board): Boolean = {
    val direction = Direction(origin, destination)
    val distance = destination.distance(origin)

    (allowed_directon(direction) && board.empty_or_opponent_at(destination, player) && distance == 1) ||
      kingSideCastling(origin, destination, board) ||
      queenSideCastling(origin, destination, board)
  }

  /**
   * The king has not previously moved.
   * The chosen rook has not previously moved.
   * There are no pieces between the king and the chosen rook.
   * The king is not currently in check.
   * The king does not pass through a square that is under attack by an enemy piece.[2]
   * The king does not end up in check (true of any legal move).
   * The king and the chosen rook are on the first rank of the player (rank 1 for White, rank 8 for Black, in algebraic notation)
   */
  def kingSideCastling(origin: Position, destination: Position, board: Board): Boolean = {
    destination == Position('g', initialPosition.rank) &&
      (!board.moves.exists(move => move.piece == this)) && // king has never moved
      (board.getPieceByPosition(Position('h', initialPosition.rank)) == Rook(player) && // rook is at its initial position
        !board.moves.exists(move => move.destination == Position('h', initialPosition.rank))) && // no piece has ever moved to rooks initial position
        !board.blocked(Direction(Position('h', initialPosition.rank), origin), origin, Position('h', initialPosition.rank)) &&
        !board.check(player) &&
        !board.reachableByOpponent(Position('f', initialPosition.rank), player) &&
        !board.reachableByOpponent(Position('g', initialPosition.rank), player)
  }
  //FIXME refactor the two methods becayse they are very similar
  def queenSideCastling(origin: Position, destination: Position, board: Board): Boolean = {
    destination == Position('c', initialPosition.rank) &&
      (!board.moves.exists(move => move.piece == this)) && // king has never moved
      (board.getPieceByPosition(Position('a', initialPosition.rank)) == Rook(player) && // rook is at its initial position
        !board.moves.exists(move => move.destination == Position('a', initialPosition.rank))) && // no piece has ever moved to rooks initial position
        !board.blocked(Direction(Position('a', initialPosition.rank), origin), origin, Position('a', initialPosition.rank)) &&
        !board.check(player) &&
        !board.reachableByOpponent(Position('d', initialPosition.rank), player) &&
        !board.reachableByOpponent(Position('c', initialPosition.rank), player)
  }
}

case class Queen(override val player: Player) extends Piece(player, if (player == White) "♕" else "♛", "Q") {
  def allowed_directon(direction: Direction): Boolean = {
    List(↖(), ↘(), ↙(), ↗(), ⇡(), ⇣(), ⇠(), ⇢()).contains(direction)
  }

  override def canMove(origin: Position, destination: Position, board: Board): Boolean = {
    val direction = Direction(origin, destination)
    allowed_directon(direction) &&
      board.empty_or_opponent_at(destination, player) &&
      !board.blocked(direction, origin, destination)
  }
}

case class Bishop(override val player: Player) extends Piece(player, if (player == White) "♗" else "♝", "B") {
  def allowed_directon(direction: Direction): Boolean = {
    List(↖(), ↘(), ↙(), ↗()).contains(direction)
  }

  override def canMove(origin: Position, destination: Position, board: Board): Boolean = {
    val direction = Direction(origin, destination)
    allowed_directon(direction) &&
      board.empty_or_opponent_at(destination, player) &&
      !board.blocked(direction, origin, destination)
  }
}

case class Rook(override val player: Player) extends Piece(player, if (player == White) "♖" else "♜", "R") {
  def allowed_directon(direction: Direction): Boolean = {
    List(⇡(), ⇣(), ⇠(), ⇢()).contains(direction)
  }
  override def canMove(origin: Position, destination: Position, board: Board): Boolean = {
    val direction = Direction(origin, destination)
    allowed_directon(direction) &&
      board.empty_or_opponent_at(destination, player) &&
      !board.blocked(direction, origin, destination)
  }
}


