package core

import core._

object Move {
  implicit class Regex(sc: StringContext) {
    def r = new util.matching.Regex(sc.parts.mkString, sc.parts.tail.map(_ => "x"): _*)
  }

  def apply(move: String, player: Player): Move = {
    move match {
      case r"([KQBRN])${ piece }([a-h]*)${ fileOfDeparture }([1-8]*)${ rankOfDeparture }([x:]*)${ capture }([a-h])${ file }([1-8])${ rank }" => {
        new Move(Piece(piece, player), Position(file.charAt(0), rank.toInt), !capture.isEmpty(), fileOfDeparture, rankOfDeparture)
      }
      case r"([a-h])${ file }([2-7])${ rank }" => {
        new Move(Pawn(player), Position(file.charAt(0), rank.toInt), false, "", "")
      }
      case r"([a-h])${ fileOfDeparture }([1-8]*)${ rankOfDeparture }[x:]([a-h])${ file }([2-7])${ rank }" => {
        new Move(Pawn(player), Position(file.charAt(0), rank.toInt), true, fileOfDeparture, rankOfDeparture)
      }
      case r"([a-h]*)${ fileOfDeparture }([a-h])${ file }8([QBRN])${ promoteTo }" if player == White => {
        new Move(Pawn(player), Position(file.charAt(0), 8), Piece(promoteTo, player), fileOfDeparture, "")
      }
      case r"([a-h]*)${ fileOfDeparture }([a-h])${ file }1([QBRN])${ promoteTo }" if player == Black => {
        new Move(Pawn(player), Position(file.charAt(0), 1), Piece(promoteTo, player), fileOfDeparture, "")
      }
      case "0-0-0" if player == White => QueenSideCastling(player, Position("c1"))

      case "0-0-0" if player == Black => QueenSideCastling(player, Position("c8"))

      case "0-0" if player == White => KingSideCastling(player, Position("g1"))

      case "0-0" if player == Black => KingSideCastling(player, Position("g8"))

      case other => null
    }
  }

  def apply(piece: Piece, destination: Position, capture: Boolean): Move = {
    new Move(piece, destination, capture, "", "")
  }

  def apply(move: Move, origin: Position): Move = {
    new Move(move.piece, move.destination, move.promoteTo, move.capture, origin.file.toString, origin.rank.toString)
  }
}

class Move(val piece: Piece, val destination: Position, val promoteTo: Piece, val capture: Boolean, val fileOfDeparture: String, val rankOfDeparture: String) {
  override def toString(): String = {
    var str = s"$piece$destination"
    if (fileOfDeparture != "" || rankOfDeparture != "")
      str += s" departing from $fileOfDeparture$rankOfDeparture"
    if (promoteTo != null)
      str += " and promotes it to a " + promoteTo
    str
  }

  def distance: Integer = {
    destination.distance(origin)
  }

  def origin: Position = {
    Position(fileOfDeparture + rankOfDeparture)
  }

  def this(piece: Piece, destination: Position, capture: Boolean) {
    this(piece, destination, null, capture, "", "")
  }

  def this(piece: Piece, destination: Position) {
    this(piece, destination, null, false, "", "")
  }

  private def this(piece: Piece, destination: Position, capture: Boolean, fileOfDeparture: String, rankOfDeparture: String) {
    this(piece, destination, null, capture, fileOfDeparture, rankOfDeparture)
  }
  private def this(piece: Piece, destination: Position, promoteTo: Piece, fileOfDeparture: String, rankOfDeparture: String) {
    this(piece, destination, promoteTo, false, fileOfDeparture, rankOfDeparture)
  }
}

case class KingSideCastling(val player: Player, override val destination: Position) extends Move(King(player), destination)
case class QueenSideCastling(val player: Player, override val destination: Position) extends Move(King(player), destination) 

//class Promote(val player: Player, override val destination: Position, override val promoteTo: Piece) extends Move(Pawn(player), destination, promoteTo, false)