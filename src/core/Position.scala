package core

import core._

object Position {
  //FIXME refactor
  implicit class Regex(sc: StringContext) {
    def r = new util.matching.Regex(sc.parts.mkString, sc.parts.tail.map(_ => "x"): _*)
  }
  def apply(direction: Direction, origin: Position, length: Int): Position = {
    direction.to(origin, length)
  }
  def apply(position: String): Position = {
    position match {
      case r"([a-h])${ file }([1-8])${ rank }" => Position(file.charAt(0), rank.toInt)
      case _ => null
    }
  }
  
  val files = 'a' to 'h' toList
  val ranks = 1 to 8 toList

  val all = for (x <- files; y <- ranks) yield Position(x, y)
}
case class Position(val file: Char, val rank: Integer) {
  //println ("rank = " +  rank)
  //require(rank >= 1 && rank <= 8, "rank should be any integer between 1 and 8")
  //require(file >= 'a' && file <= 'h', "file should be any char from 'a' to 'h'")

  override def toString(): String = file + rank.toString()

  def ⇡(len: Int = 1): Position = {
    Position(file, rank + len)
  }

  def ⇣(len: Int = 1): Position = {
    Position(file, rank - len)
  }

  def ⇠(len: Int = 1): Position = {
    Position((file - len).toChar, rank)
  }

  def ⇢(len: Int = 1): Position = {
    Position((file + len).toChar, rank)
  }

  def distance(other: Position): Int = {
    if (file == other.file)
      Math.abs(rank - other.rank)
    else
      Math.abs(file - other.file)
  }
  
  def positionsTo(destination: Position) = {
    (1 to (destination.distance(this) - 1)).map(offset => Position(Direction(this, destination), this, offset))
  }
  
  def positionsToInclusive(destination: Position) = {
    (1 to (destination.distance(this))).map(offset => Position(Direction(this, destination), this, offset))
  }

}
