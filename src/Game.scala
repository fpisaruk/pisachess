import scala.annotation.tailrec
import core._
import core.Player

object Game extends App {
  @tailrec def play(board: core.Board, player: Player) {
    println(board)
    println("Moves: " + (board.moves mkString ", "))
    if (board.checkMate(player)) {
      if (player == White)
        println("0-1")
      else
        println("1-0")
      return
    }
    print(player + "> ")
    val move = Move(Console.readLine, player)
    if (move == null) {
      println("invalid move!")
      play(board, player)
    } else {
      println(move)
      if (board.checkMove(move)) {
        val newBoard = board.applyMove(move)
        if (newBoard.check(player)) {
          println("You cannnot do this move because your King is under check or will be in check")
          play(board, player)
        } else {
          play(newBoard, player opponent)
        }
      } else
        play(board, player)
    }
  }

  print("Welcome to Pisaruk Chess");
  play(Board(), White)
}
