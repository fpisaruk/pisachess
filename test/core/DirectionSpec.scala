package core

import org.scalatest.FunSpec

class DirectionSpec extends FunSpec {

  describe("A Direction") {

    it("should be constructed based on origin and destination") {
      val origin = Position('c',2)
      assert(Direction(origin, Position('c',3)) == ⇡())
      assert(Direction(origin, Position('d',3)) == ↗())
      assert(Direction(origin, Position('d',2)) == ⇢())
      assert(Direction(origin, Position('d',1)) == ↘())
      assert(Direction(origin, Position('c',1)) == ⇣())
      assert(Direction(origin, Position('b',1)) == ↙())
      assert(Direction(origin, Position('b',2)) == ⇠())
      assert(Direction(origin, Position('b',3)) == ↖())
    }
  }
}