package core

import org.scalatest.FunSpec
import org.scalatest.BeforeAndAfter

class PieceSpec extends FunSpec with BeforeAndAfter {
  describe("King") {
    describe("castling") {
      val iniboard = Board(Map(Position("e1") -> King(White),
        Position("h1") -> Rook(White),
        Position("a1") -> Rook(White)))

      describe("kingside") {
        it("is allowed when king and rook has not moved and during the castling path king is not under check") {
          val board = iniboard.applyMove(Move("Ra2", White))
          assert(King(White).canMove(Position("e1"), Position("g1"), board))
        }
        it("is not allowed when path between king and rook is not empty") {
          val board = Board(Map(Position("e1") -> King(White), Position("f1") -> Bishop(White), Position("h1") -> Rook(White)))
          assert(!King(White).canMove(Position("e1"), Position("g1"), board))
        }
        it("is not allowed when king is not at its initial position") {
          val board = iniboard.applyMove(Move("Kd1", White))
          assert(!King(White).canMove(Position("d1"), Position("g1"), board))
        }
        it("is not allowed when rook is not at its initial position") {
          val board = iniboard.applyMove(Move("Rh2", White))
          assert(!King(White).canMove(Position("e1"), Position("g1"), board))
        }
        it("is not allowed when rook has moved back to its initial positionn") {
          val board = iniboard.applyMove(Move("Rh2", White)).
            applyMove(Move("Rh1", White))
          assert(!King(White).canMove(Position("e1"), Position("g1"), board))
        }
        it("is not allowed when king has moved back to its initial position") {
          val board = iniboard.applyMove(Move("Ke2", White)).
            applyMove(Move("Ke1", White))
          assert(!King(White).canMove(Position("e1"), Position("g1"), board))
        }
        it("is not allowed when king is under check") {
          val board = Board(Map(Position("e1") -> King(White), Position("h1") -> Rook(White), Position("e8") -> Queen(Black)))
          assert(!King(White).canMove(Position("e1"), Position("g1"), board))
        }
        it("is not allowed when king will be under check during intermediate castling position") {
          val board = Board(Map(Position("e1") -> King(White), Position("h1") -> Rook(White), Position("f8") -> Queen(Black)))
          assert(!King(White).canMove(Position("e1"), Position("g1"), board))
        }
        it("is not allowed when king will be under check during final castling position") {
          val board = Board(Map(Position("e1") -> King(White), Position("h1") -> Rook(White), Position("g8") -> Queen(Black)))
          assert(!King(White).canMove(Position("e1"), Position("g1"), board))
        }
      }

      describe("queenside") {
        it("is allowed when king and rook has not moved and during the castling path king is not under check") {
          val board = iniboard.applyMove(Move("Rh2", White))
          assert(King(White).canMove(Position("e1"), Position("c1"), board))
        }
        it("is not allowed when path between king and rook is not empty. ex: a bishop is between then") {
          val board = Board(Map(Position("e1") -> King(White), Position("d1") -> Bishop(White), Position("a1") -> Rook(White)))
          assert(!King(White).canMove(Position("e1"), Position("c1"), board))
        }
        it("is not allowed when path between king and rook is not empty. ex: a knight is between then") {
          val board = Board(Map(Position("e1") -> King(White), Position("b1") -> Knight(White), Position("a1") -> Rook(White)))
          println(board)
          assert(!King(White).canMove(Position("e1"), Position("c1"), board))
        }
        it("is not allowed when king is not at its initial position") {
          val board = iniboard.applyMove(Move("Kf1", White))
          assert(!King(White).canMove(Position("f1"), Position("c1"), board))
        }
        it("is not allowed when rook is not at its initial position") {
          val board = iniboard.applyMove(Move("Ra2", White))
          assert(!King(White).canMove(Position("e1"), Position("c1"), board))
        }
        it("is not allowed when rook has moved back to its initial positionn") {
          val board = iniboard.applyMove(Move("Ra2", White)).
            applyMove(Move("Ra1", White))
          assert(!King(White).canMove(Position("e1"), Position("c1"), board))
        }
        it("is not allowed when king has moved back to its initial position") {
          val board = iniboard.applyMove(Move("Ke2", White)).
            applyMove(Move("Ke1", White))
          assert(!King(White).canMove(Position("e1"), Position("c1"), board))
        }
        it("is not allowed when king is under check") {
          val board = Board(Map(Position("e1") -> King(White), Position("a1") -> Rook(White), Position("e8") -> Queen(Black)))
          assert(!King(White).canMove(Position("e1"), Position("c1"), board))
        }
        it("is not allowed when king will be under check during intermediate castling position") {
          val board = Board(Map(Position("e1") -> King(White), Position("a1") -> Rook(White), Position("d8") -> Queen(Black)))
          assert(!King(White).canMove(Position("e1"), Position("c1"), board))
        }
        it("is not allowed when king will be under check during final castling position") {
          val board = Board(Map(Position("e1") -> King(White), Position("a1") -> Rook(White), Position("c8") -> Queen(Black)))
          assert(!King(White).canMove(Position("e1"), Position("c1"), board))
        }
      }
    }
  }
}
