package core

import org.scalatest.FunSpec

class MoveSpec extends FunSpec {
  describe("A Move") {

    it("can specify fileOfDeparture"){
      val move = Move("Ngf3", Black)
      assert(move.piece == Piece("♞"))
      assert(move.fileOfDeparture == "g")
    }
    
    it("can specify rankOfDeparture"){
      val move = Move("N1f3", Black)
      assert(move.piece == Piece("♞"))
      assert(move.rankOfDeparture == "1")
    }
    
    it("can specify file and rank of departure"){
      val move = Move("Ng1f3", Black)
      assert(move.piece == Piece("♞"))
      assert(move.fileOfDeparture == "g")
      assert(move.rankOfDeparture == "1")
    }
    
    it("can be a white pawn promotion") {
      val move = Move("e8Q", White)
      assert(move.promoteTo == Piece("♕"))
    }

    it("can be a black pawn promotion") {
      val move = Move("e1Q", Black)
      assert(move.promoteTo == Piece("♛"))
    }

    it("is invalid when promotion piece is not supplied and player is white and destination rank is 8") {
      assert(Move("e8", White) == null)
    }

    it("is invalid when promotion piece is not supplied and player is black and destination rank is 1") {
      assert(Move("e1", Black) == null)
    }

    describe("Captures") {
      it("can be a white pawn capture") {
        val move = Move("axd3", White)
        assert(move.capture)
        assert(move.promoteTo == null)
        assert(move.destination == Position('d', 3))
        assert(move.piece == Piece("♙"))
      }

      it("can be a black pawn capture") {
        val move = Move("axd3", Black)
        assert(move.capture)
        assert(move.promoteTo == null)
        assert(move.destination == Position('d', 3))
        assert(move.piece == Piece("♟"))
      }
      
      it("can be a white queen capture") {
      val move = Move("Qxa1", White)
      assert(move.capture)
      assert(move.promoteTo == null)
      assert(move.destination == Position('a', 1))
      assert(move.piece == Piece("♕"))
    }
      
    }

    it("can be a white pawn movement") {
      val move = Move("a3", White)
      assert(move.promoteTo == null)
      assert(move.destination == Position('a', 3))
      assert(move.piece == Piece("♙"))
    }

    it("can be a white queen movement") {
      val move = Move("Qa1", White)
      assert(move.promoteTo == null)
      assert(move.capture==false)
      assert(move.destination == Position('a', 1))
      assert(move.piece == Piece("♕"))
    }

    it("can be a white king movement") {
      val move = Move("Ka1", White)
      assert(move.promoteTo == null)
      assert(move.destination == Position('a', 1))
      assert(move.piece == Piece("♔"))
    }
    it("can be a white rook movement") {
      val move = Move("Ra1", White)
      assert(move.promoteTo == null)
      assert(move.destination == Position('a', 1))
      assert(move.piece == Piece("♖"))
    }

    it("can be a white bishop movement") {
      val move = Move("Ba1", White)
      assert(move.promoteTo == null)
      assert(move.destination == Position('a', 1))
      assert(move.piece == Piece("♗"))
    }

    it("can be a white knight movement") {
      val move = Move("Na1", White)
      assert(move.promoteTo == null)
      assert(move.destination == Position('a', 1))
      assert(move.piece == Piece("♘"))
    }

    it("can be a black pawn movement") {
      val move = Move("a6", Black)
      assert(move.promoteTo == null)
      assert(move.destination == Position('a', 6))
      assert(move.piece == Piece("♟"))
    }

    it("can be a black queen movement") {
      val move = Move("Qa1", Black)
      assert(move.promoteTo == null)
      assert(move.destination == Position('a', 1))
      assert(move.piece == Piece("♛"))
    }

    it("can be a black king movement") {
      val move = Move("Ka1", Black)
      assert(move.promoteTo == null)
      assert(move.destination == Position('a', 1))
      assert(move.piece == Piece("♚"))
    }

    it("can be a black rook movement") {
      val move = Move("Ra1", Black)
      assert(move.promoteTo == null)
      assert(move.destination == Position('a', 1))
      assert(move.piece == Piece("♜"))
    }

    it("can be a black bishop movement") {
      val move = Move("Ba1", Black)
      assert(move.promoteTo == null)
      assert(move.destination == Position('a', 1))
      assert(move.piece == Piece("♝"))
    }

    it("can be a black knight movement") {
      val move = Move("Na1", Black)
      assert(move.promoteTo == null)
      assert(move.destination == Position('a', 1))
      assert(move.piece == Piece("♞"))
    }

    it("is invalid when second and third characters are not valid file and ranks respectively") {
      assert(Move("Na9", Black) == null)
      assert(Move("Na0", Black) == null)
      assert(Move("Nj9", Black) == null)
      assert(Move("Ni9", Black) == null)
    }

    it("is invalid when first character does not represents a valid piece") {
      assert(Move("Xa1", Black) == null)
      assert(Move("Ya1", Black) == null)
      assert(Move("Za1", Black) == null)
    }

  }
}