package core

import org.scalatest.FunSpec

class BoardSpec extends FunSpec {
  describe("Board") {
    describe("applyMove") {
      describe("castling") {
        val board = Board(Map(Position("e1") -> King(White),
          Position("h1") -> Rook(White),
          Position("a1") -> Rook(White)))
        it("executes kingside castling") {
          val move = Move("0-0", White)
          val newBoard = board.applyMove(move)
          assert(newBoard.getPieceByPosition(Position("g1")) == King(White))
          assert(newBoard.empty(Position("e1")))
          assert(newBoard.empty(Position("h1")))
          assert(newBoard.getPieceByPosition(Position("f1")) == Rook(White))
        }
        it("executes queenside castling") {
          val move = Move("0-0-0", White)
          val newBoard = board.applyMove(move)
          assert(newBoard.getPieceByPosition(Position("c1")) == King(White))
          assert(newBoard.empty(Position("e1")))
          assert(newBoard.empty(Position("a1")))
          assert(newBoard.getPieceByPosition(Position("d1")) == Rook(White))
        }
      }
      describe("en passant") {
        it("is allowed when opponent pawn has just moved two positions ahead") {
          val board = Board(Map(Position("f7") -> Pawn(Black), Position("e5") -> Pawn(White))).
            applyMove(Move("f5", Black)).applyMove(Move("exf6", White))
          assert(board.empty(Position("f5")))
          assert(board.empty(Position("e5")))
          assert(board.getPieceByPosition(Position("f6")) == Pawn(White))
        }
        it("is not allowed when opponent pawn has moved two positions ahead more than one turn ago") {
          val board = Board(Map(Position("a5") -> Pawn(White), Position("f7") -> Pawn(Black), Position("e5") -> Pawn(White))).
            applyMove(Move("f5", Black)).
            applyMove(Move("a6", White))
          assert(!board.checkMove(Move("exf6", White)))
        }
      }
      describe("move when more than one piece can move to the same square but piece was uniquely specified") {
        it("two knights can move to the same destination") {
          val board = Board(Map(
            Position("g1") -> Knight(Black),
            Position("d2") -> Knight(Black)))
          val newBoard = board.applyMove(Move("Ngf3", Black))
          assert(newBoard.getPieceByPosition(Position("f3")) == Knight(Black))
          assert(newBoard.empty(Position("g1")))
          assert(newBoard.getPieceByPosition(Position("d2")) == Knight(Black))
        }

        it("two pawn can capture the same opponent piece") {
          val board1 = Board(Map(
            Position("a4") -> Pawn(White),
            Position("c4") -> Pawn(White),
            Position("b5") -> Pawn(Black)))
          val newBoard1 = board1.applyMove(Move("axb5", White))
          assert(newBoard1.getPieceByPosition(Position("b5")) == Pawn(White))
          assert(newBoard1.empty(Position("a4")))
          assert(newBoard1.getPieceByPosition(Position("c4")) == Pawn(White))
        }

        it("two pawns can be promoted to the same destination") {
          val board1 = Board(Map(
            Position("a7") -> Pawn(White),
            Position("c7") -> Pawn(White),
            Position("b8") -> Queen(Black)))
          assert(!board1.checkMove(Move("b8Q", White)))
          val newBoard1 = board1.applyMove(Move("cb8Q", White))
          assert(newBoard1.getPieceByPosition(Position("b8")) == Queen(White))
          assert(newBoard1.empty(Position("c7")))
          assert(newBoard1.getPieceByPosition(Position("a7")) == Pawn(White))

        }
      }
    }

    describe("checkMove") {
      it("does not move when more than one piece can move to the same square") {
        val board = Board(Map(
          Position("g1") -> Knight(Black),
          Position("d2") -> Knight(Black)))
        assert(!board.checkMove(Move("Nf3", Black)))
      }

      it("move when more than one piece can move to the same square but piece was uniquely specified") {
        val board = Board(Map(
          Position("g1") -> Knight(Black),
          Position("d2") -> Knight(Black)))
        assert(board.checkMove(Move("Ngf3", Black)))
      }
    }
    describe("king is under check") {
      it("when any opponent piece can attack it") {
        assert(Board(Map(
          Position("a1") -> Queen(Black),
          Position("h8") -> King(White))).check(White))
      }
    }
    describe("its is a checkmate") {
      it("there is no move after which king is not under check") {
        assert(!Board(Map(
          Position("a8") -> Queen(Black),
          Position("d7") -> Bishop(White),
          Position("c7") -> Pawn(White),
          Position("a1") -> Rook(White),
          Position("b6") -> Pawn(White),
          Position("e8") -> King(White))).checkMate(White))
      }
    }
    //FIXME it is not good because it is allowing the pawn to move to the last rank without informing the promotion
    describe("it is not a checkmate if a promoted pawn blocks the opponent attack") {
      val board = Board(Map(
        Position("a8") -> Queen(Black),
        Position("d7") -> Pawn(White),
        Position("e7") -> Pawn(White),
        Position("f7") -> Pawn(White),
        Position("e8") -> King(White)))
      println(board)
      assert(!board.checkMate(White))
    }
  }
}